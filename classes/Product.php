<?php


class Product extends BaseClass
{
    public $name;

    public $price;

    public $category_id;

    public $discount;

    public $image;

    public $code;

    public $review_number;

    public static function getTableName()
    {
        return 'products';
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



}