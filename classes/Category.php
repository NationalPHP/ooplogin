<?php


class Category
{
    public $id;

    public $name;

    public $products = [];

    /**
     * Category constructor.
     * @param $id
     */
    public function __construct($id)
    {
        $data = findOneBy('categories','id', $id);
        $this->id = $id;
        $this->name = $data['name'];

        $productsData = findBy('products', 'category_id', $id);
        foreach ($productsData as $productData){
            $this->products[]= new Product($productData['id']);
        }
    }


}