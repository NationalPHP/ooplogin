<?php


class EWProduct extends Product
{
   public $percent;

   public $parent_product_id;

    public function getPrice()
    {
        $parentPrice = Product::find($this->parent_product_id)->getPrice();

        return $parentPrice * $this->percent / 100;
    }

    /**
    * @return mixed
    */
    public function getName()
    {
        $parentName = Product::find($this->parent_product_id)->getName();
        return $this->name."($parentName)";
    }


}