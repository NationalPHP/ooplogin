<?php


class Car extends BaseClass
{
    public $maker_id;

    public $model;

    public $price;

    public $year;

    public $km;

    public $hp;

    public $fuel_type;

    public $city;

    public $state;

    public $type;

    public $user_id;

    public function getUser()
    {
        return User::find($this->user_id);
    }


    public function getMaker()
    {
        return Maker::find($this->maker_id);
    }

    public function getCarImages()
    {
        return CarImage::findBy('car_id', $this->id);
    }

    public function getDiscount()
    {
        if ($this->getMaker()->name == 'Mercedes'){
            return 20;
        } else {
            return 10;
        }

    }

    public function getPrice()
    {
        return intval($this->price - ($this->price * ($this->getDiscount()/100)));
    }

    public static function getTableName()
    {
        return 'cars';
    }

}