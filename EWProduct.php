<?php


class EWProduct extends Product
{
   public $procent;

   public $parent_product_id;

    public function getPrice()
    {
        $parentPrice = Product::find($this->parent_product_id);
        return $parentPrice * $this->procent / 100;
    }


}