<?php
include "functions.php";

$user = User::findOneBy('email', $_POST['email']);

if (is_null($user)){
    $_SESSION['error_message'] = 'Nu exista userul!';
    header('Location: login.php');
} else {
    if (md5($salt.$_POST['password'].$salt)==$user->password){
        $_SESSION['success_message'] = 'Autentificare reusita!';
        $_SESSION['user_id'] = $user->id;
        header('Location: secured.php');
    } else {
        $_SESSION['error_message'] = 'Parola gresita!';
        header('Location: login.php');
    }
}