<?php
include "functions.php";

if (strlen($_POST['password'])<6){
    $_SESSION['error_message'] = 'Parola e prea scurta. Minim 6 caractere';
    header('Location: register.php');
    die;
}

if ($_POST['password']!=$_POST['retype_password']){
    $_SESSION['error_message'] = 'Parolele nu corespund';
    header('Location: register.php');
    die;
}

if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
    $_SESSION['error_message'] = 'Emailul nu este valid';
    header('Location: register.php');
    die;
}

$userExists = User::findOneBy('email', $_POST['email']);

if (!is_null($userExists)){
    $_SESSION['error_message'] = 'Emailul a mai fost folosit';
    header('Location: register.php');
    die;
}
$data = $_POST;
unset($data['retype_password']);
$user = new User();
$user->fromArray($data);
$user->password = md5($salt.$user->password.$salt);
$user->save();
$_SESSION['user_id'] = $user->id;
header('Location: secured.php');